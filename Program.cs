﻿
using System;

public class MockDbContext{
    public string DataFromDataBase { get; set; } = "It Worx!";
}

public interface IRequest { }
public interface IResponse { }

public interface IRequestHandler<IRequest, IResponse>
{

}

public class Program
{
    public static async System.Threading.Tasks.Task Main()
    {
        var r = new TenantDeleteRequest { Id = 0 };


        var tenantHandler = new TenantHandler(new MockDbContext { });

        var response = await tenantHandler.Handle(r, new System.Threading.CancellationToken() );


        Console.WriteLine(response);

    }
}