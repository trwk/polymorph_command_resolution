﻿using System.Threading.Tasks;

public abstract class TenantRequest<Out, D> : IRequest 
{
    public abstract Task<Out> Invoke(D dependencies);
}
