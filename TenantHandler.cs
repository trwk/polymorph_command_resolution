﻿using System.Threading;
using System.Threading.Tasks;
using static TenantHandler;

using TenantRequestHandler = TenantRequest<TenantResponse, TenantHandler.TenantDependencies>;

public class TenantHandler : 
    IDependencies<TenantDependencies>,
    IRequestHandler<TenantRequestHandler, TenantResponse>
{
    public TenantDependencies Dependencies { get; set; }

    public class TenantDependencies
    {
        public MockDbContext Context { get; set; }
    } 

    public TenantHandler(MockDbContext context)
    {
        Dependencies = new TenantDependencies
        {
            Context = context
        };
    }

    public Task<TenantResponse> Handle(TenantRequestHandler requestHandler, CancellationToken cancellationToken)
        => requestHandler.Invoke(Dependencies);
}
