﻿using System.Threading.Tasks;
using static TenantHandler;

public class TenantDeleteRequest : TenantRequest<TenantResponse, TenantDependencies>, IRequest
{
    public int Id { get; set; }

    public override Task<TenantResponse> Invoke(TenantDependencies dependencies)
    {
        var databasedata = dependencies.Context.DataFromDataBase;
        var r = (TenantResponse)new TenantDeleteResponse { Message = databasedata };
        return Task.FromResult(r);
    }
}
