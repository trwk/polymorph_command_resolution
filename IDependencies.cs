﻿public interface IDependencies<T> where T: new()
{
    public T Dependencies { get; set; }
}
