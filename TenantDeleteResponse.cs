﻿public class TenantDeleteResponse : TenantResponse
{
    public string Message { get; set; }
}
